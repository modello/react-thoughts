/**
 * @jsx React.DOM
 */
var React = require('react/addons');
var _ = require('underscore');
var ThoughtsActions = require('../actions/ThoughtsActions');
var RouterActions = require('../actions/RouterActions');

var ThoughtsDetail = React.createClass({

	propTypes: {
		title: React.PropTypes.string.isRequired,
		description: React.PropTypes.string.isRequired,
		tags: React.PropTypes.string
	},

	getInitialState: function() {
		return { 
			title: this.props.title,
			id: this.props.id,
			description: this.props.description,
			tags: this.props.tags
		};
	},

	componentWillReceiveProps: function(nextProps) {
		this.setState({
			title: nextProps.title,
			description: nextProps.description,
			tags: nextProps.tags,
			id: nextProps.id,
			changed: nextProps.id === this.props.id
		});
	},

	onChangeTitle: function(event) {
		ThoughtsActions.set(this.state.id, {title: event.target.value});
	},

	onChangeDescription: function(event) {
		ThoughtsActions.set(this.state.id, {description: event.target.value});
	},

	onChangeTags: function(event) {
		ThoughtsActions.set(this.state.id, {tags: event.target.value});
	},

	onClickDelete: function() {
		ThoughtsActions.destroy(this.state.id, this.props);
		RouterActions.navigate('thoughts');	
	},

	onClickSave: function() {
		ThoughtsActions.save(this.state.id, this.props);
		this.setState({ changed: false });
	},

	render: function() {

		var cx = React.addons.classSet;
		var saveBtnClasses = cx({
		    'btn': true,
		    'btn-default': true,
		    'disabled': !this.state.changed
		});

		return (
			<div className="row">
				<div className="col-sm-12 col-md-12">
					<div className="row">
						<div className="page-header col-sm-12 col-md-12">
							<button id="removeBtn" type="button" className="btn btn-default pull-right" onClick={this.onClickDelete}><span className="glyphicon glyphicon-minus"></span></button>
						</div>
					</div>
				</div>
				<div className="col-sm-12 col-md-12">
					<div className="row">
						<div className="col-sm-12 col-md-12">
							
							<form className="form-horizontal" role="form">
								<div className="form-group">
									<label htmlFor="inputTitle" className="col-sm-2 control-label">Title</label>
									<div className="col-sm-10">
										<input id="inputTitle" type="text" className="form-control" placeholder="title here" value={this.state.title} onChange={this.onChangeTitle}></input>
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="inputDetails" className="col-sm-2 control-label">Details</label>
									<div className="col-sm-10">
										<textarea id="inputDetails" placeholder="details" className="form-control" rows="3" value={this.state.description} onChange={this.onChangeDescription}></textarea>
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="inputTags" className="col-sm-2 control-label">Tags</label>
									<div className="col-sm-10">
										<input id="inputTags" type="text" className="form-control" placeholder="tags here (comma delimited)" value={this.state.tags} onChange={this.onChangeTags}></input>
									</div>
								</div>
								<div className="text-right">
									<button type="button" className={saveBtnClasses} onClick={this.onClickSave}>Save</button>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = ThoughtsDetail;