/**
 * @jsx React.DOM
 */
var React = require('react');
var ThoughtsDetail = require('./ThoughtsDetail');
var ThoughtsList = require('./ThoughtsList');
var _ = require('underscore');
var ThoughtsActions = require('../actions/ThoughtsActions');
var RouterActions = require('../actions/RouterActions');

var ThoughtsScreen = React.createClass({

  getInitialState: function() {
      return this.updateState(this.props);
  },

  componentWillReceiveProps: function(nextProps) {
    this.setState(this.updateState(nextProps));
  },

  updateState: function(props) {
    return {
      selectedId: props.selectedId,
      selectedTag: props.selectedTag,
      thoughts: props.selectedTag? _.filter(props.thoughts, function(thought) {
        return _.contains(thought.tags.split(','), props.selectedTag);
      }) : props.thoughts
    };
  },

  render: function() {

    var selectedItem;

    var detailNode = 'Select a thought from the menu';
    var filterNode = '';

    if(this.state.selectedTag) {
      filterNode = <button className="btn btn-success" onClick={this._onRemoveFilter}>
          <span className="glyphicon-class ng-binding">{this.state.selectedTag}</span>
          <span className="glyphicon glyphicon-remove-circle"></span>
      </button>;
    }

    if(this.state.selectedId) {
      selectedItem = _.findWhere(this.props.thoughts, {id: this.state.selectedId});
      detailNode = <ThoughtsDetail {...selectedItem} />;
    }

    return (
      <div>
        <div className="page-header">
          <h1>Thoughts</h1>
        </div>
        <div className="row">
          <div className="col-md-3">
            <div className="page-header">
              <button id="addThoughtBtn" type="button" className="btn btn-default" onClick={this._onAddItem}>
                <span className="glyphicon glyphicon-plus"></span>
              </button>
              {filterNode}
            </div>
            <div id="listContainer"><ThoughtsList thoughts={this.state.thoughts} onSelect={this._onSelectItem} selectedId={this.state.selectedId} /></div>
          </div>
          <div id="detailContainer" className="col-md-9">{detailNode}</div>
        </div>
      </div>
    );
  },

  _onSelectItem: function(e, id) {

    if(this.state.selectedTag) {
      RouterActions.navigate('thoughts/'+ id + '?tag='+this.state.selectedTag); 
    }else {
      RouterActions.navigate('thoughts/'+id); 
    }

  },

  _onAddItem: function() {
    ThoughtsActions.create();
  },

  _onRemoveFilter: function() {

    if(this.state.selectedTag && this.state.selectedId) {
      RouterActions.navigate('thoughts/'+this.state.selectedId); 
    }else {
      RouterActions.navigate('thoughts'); 
    }
  }


});

module.exports = ThoughtsScreen;