/**
 * @jsx React.DOM
 */
var React = require('react');
var ThoughtItem = require('./ThoughtItem');
var _ = require('underscore');

var ThoughtsList = React.createClass({

	propTypes: {
		selectedId: React.PropTypes.string
	},

	render: function() {

		var node = _(this.props.thoughts).map(function(thought, i) {
			return <ThoughtItem {...thought} key={thought.id} active={this.props.selectedId===thought.id} onSelectItem={this._onSelectItem} />;
		}, this);

		return (
			<div className="list-group">{node}</div>
		);
	},

	_onSelectItem: function(e, id) {

		var modelId = id.substr(id.lastIndexOf('$')+1, id.length);
		this.props.onSelect(e, modelId);

	}
});

module.exports = ThoughtsList;