var AppDispatcher = require('./dispatcher/AppDispatcher');
var Thoughts = require('./stores/Thoughts');
var Tags = require('./stores/Tags');
var Router = require('./Router');

var router = new Router({ dispatcher: AppDispatcher });

var thoughtsStore = new Thoughts([], { dispatcher: AppDispatcher, router: router });
thoughtsStore.fetch()

.always(function(){
	if(!thoughtsStore.length) {
		thoughtsStore.set([
			{
				id: '123',
				title: 'Manchester United troubles \'affect Premier League brand\'',
				description: 'definately a title from the BBC website',
				tags: 'football,manu'
			},
			{
				id: '456',
				title: 'Liverpool boss says Man City still title favourites',
				description: 'I agree but hopefully not!',
				tags: 'football,mancity,liverpool'
			}
		]);
		thoughtsStore.each(function(m) { m.save(); });
	}
});


var tagsStore = new Tags([], { dispatcher: AppDispatcher });
tagsStore.fetch()

.always(function() {
	if(!tagsStore.length) {
		tagsStore.set([
			{ label:'football', thoughtIds:['123', '456'] },
			{ label:'manu', thoughtIds:['123'] },
			{ label:'mancity', thoughtIds:['456'] },
			{ label:'liverpool', thoughtIds:['456'] }
		]);
		tagsStore.each(function(m) { m.save(); });
	}
});


module.exports = {
	thoughtsStore: thoughtsStore,
	tagsStore: tagsStore,
	router: router
};