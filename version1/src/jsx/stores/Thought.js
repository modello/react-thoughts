var Backbone = require('backbone');

module.exports = Backbone.Model.extend({

	defaults: {
		title: 'A title',
		description: 'Details of my other thought..',
		tags: ''
	}

});