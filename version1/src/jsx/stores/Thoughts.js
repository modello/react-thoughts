var ThoughtConstants = require('../constants/ThoughtConstants');
var Backbone = require('backbone');
var Thought = require('./Thought');
require('backbone.localstorage');
Backbone.$ = require('jquery');

module.exports = Backbone.Collection.extend({

	model: Thought,

	localStorage: new Backbone.LocalStorage("Thoughts"),

	initialize: function(opt, options) {
		
		this.router = options.router;
		this.dispatcher = options.dispatcher;
		this.dispatcher.register(this.onDispatcher.bind(this));

	},

	// Register to handle all updates
	onDispatcher: function(payload) {

		var action = payload.action;

		switch(action.actionType) {
			case ThoughtConstants.THOUGHT_CREATE:
				var model = this.create({
					title: 'Here\'s another thought ('+(this.length+1)+')'
				}, { at: 0 });
				this.router.navigate('thoughts/'+model.id, {trigger: true});//todo: move out of store
				break;
			case ThoughtConstants.THOUGHT_SAVE:
				this.get(action.id).save();
				break;
			case ThoughtConstants.THOUGHT_SET:
				this.get(action.id).set(action.attributes);
				break;
			case ThoughtConstants.THOUGHT_DESTROY:
				this.get(action.id).destroy();
				break;
			case ThoughtConstants.THOUGHTS_FETCH:
				this.fetch();
				break;

			default:
			  return true;
		}

		return true; // No errors.  Needed by promise in Dispatcher.

	}

});