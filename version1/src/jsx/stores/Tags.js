var Backbone = require('backbone');
var _ = require('underscore');
var Tag = require('./Tag');
var ThoughtConstants = require('../constants/ThoughtConstants');
require('backbone.localstorage');
Backbone.$ = require('jquery');

module.exports = Backbone.Collection.extend({

	model: Tag,

	localStorage: new Backbone.LocalStorage('Tags'),

	initialize: function(opt, options) {
		
		this.dispatcher = options.dispatcher;
		this.dispatcher.register(this.onDispatcher.bind(this));

	},

	// Register to handle all updates
	onDispatcher: function(payload) {

		var action = payload.action;
		
		switch(action.actionType) {
			case ThoughtConstants.THOUGHT_SAVE:
			
				this.saveTags(action.model);
				this.cleanUpOldRefsToThought(action.model);
				this.removeTagsWithNoThoughts(action.model);
				break;
			case ThoughtConstants.THOUGHT_DESTROY:

				this.removeRefToThought(action.model);
				this.removeTagsWithNoThoughts(action.model);
				break;
			default:
			  return true;
		}

		return true; //Needed by promise in Dispatcher

	},

	removeRefToThought: function(thought) {

		this.each(function(tag) {

			var isThoughtInTag = _.contains(tag.get('thoughtIds'), thought.id);

			if(isThoughtInTag) {
				tag.set('thoughtIds', _.difference(tag.get('thoughtIds'), [thought.id]));
			}

		}, this);

	},

	cleanUpOldRefsToThought: function(thought) {

		this.each(function(tag) {
			var isThoughtInTag = _.contains(tag.get('thoughtIds'), thought.id);
			var isTagInThought = _.contains(thought.tags.slice().split(','), tag.get('label'));

			if(isThoughtInTag && !isTagInThought) {
				tag.set('thoughtIds', _.difference(tag.get('thoughtIds'), [thought.id]));
			}

		}, this);

	},

	removeTagsWithNoThoughts: function(thought) {

		var oldTags = this.filter(function(tag) {
			return tag.get('thoughtIds').length === 0;
		}, this);

		_.each(oldTags, function(tag) {
			tag.destroy();
		});

		this.remove(oldTags);

	},

	addItem: function(item) { 
		
		var tag = this.findWhere({ label: item.label });
		if(tag) {
			if(!item.thoughtIds) {return tag;}

			//update existing tag
			tag.set('thoughtIds', _.union(tag.get('thoughtIds'), item.thoughtIds));
			tag.save();

		}else {
			//create new
			this.create(item);
		}
	},

	//add or update tags
	saveTags: function(thought) {

		if(thought.tags.length === 0) { return; }

		var thoughtTags = thought.tags.slice().split(',');
		
		_.each(thoughtTags, function(label) {
			var tag = this.addItem({ label: label, thoughtIds: [thought.id] });
		}, this);

	}

});