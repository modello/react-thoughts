var AppDispatcher = require('../dispatcher/AppDispatcher');
var ThoughtConstants = require('../constants/ThoughtConstants');

var ThoughtsActions = {

	create: function() {
		AppDispatcher.onViewAction({
			actionType: ThoughtConstants.THOUGHT_CREATE
		});
	},
	set: function(id, attributes) {
		AppDispatcher.onViewAction({
			actionType: ThoughtConstants.THOUGHT_SET,
			id: id,
			attributes: attributes
		});
	},
	save: function(id, model) {
		AppDispatcher.onViewAction({
			actionType: ThoughtConstants.THOUGHT_SAVE,
			id: id,
			model: model
		});
	},
	destroy: function(id, model) {
		AppDispatcher.onViewAction({
			actionType: ThoughtConstants.THOUGHT_DESTROY,
			id: id,
			model: model
		});
	}
};

module.exports = ThoughtsActions;
