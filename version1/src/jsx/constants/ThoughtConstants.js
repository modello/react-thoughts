var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
	THOUGHTS_FETCH: null,
	THOUGHT_CREATE: null,
	THOUGHT_SET: null,
	THOUGHT_SAVE: null,
	THOUGHT_DESTROY: null
});