reactjs-thoughts-app
====================

Similar demo to https://github.com/Fintan/angular-thoughts-app but using Reactjs with Backbone Models, Collections and Routers as part of a Flux architecture

##Description:

A simple application for compiling thoughts.

Demo ([http://fboyle.com/demo/react-thoughts/#thoughts/123] [demo_link])

##Future Updates:

Whilst I do like version 1 of this demo app, such as following the basic Flux architecture and leveraging Backbone models and collections and also treating the Backbone Router as a store, there are still areas to improve.

I'm probably going to create another version of this app where stores will have their own dispatchers rather than having a singleton dispatcher.  I also may look at using Backbone.Radio for dispatching.  I may also revise how the app responds to route events, maybe creating another store that describes application state rather than setting them directly on the main React view's state object.  Then the main React view can subscribe to that store in order to update its state property.  State updates resulting from user interactions can also then be set on that state store (via the unidirectional flow) and the route can be navigated to without having to set 'trigger' to true.

I also need to investigate the 'context' property on React views for setting dependencies.

## Installation

Retrieve third party dependencies using **npm install** 
Build app from version1 directory by running **grunt dev**

## Contributors

[fintangit_www]: https://github.com/Fintan
Fintan Boyle ([https://github.com/Fintan] [fintangit_www])


## License

MIT